
 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   mult_signed_32x32_stall.v
//Module name   :   mult_signed_32x32_stall
//Full name     :   32x32 signed mulitplier with stall 
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------  
`include "marco.v" 
module mult_signed_32x32_stall
(
    input                   clk,
    input                   rst_n,
    input                   busy,
    input                   en,
    input[31:0]             dina,
    input[31:0]             dinb,
    output[63:0]            dout ,
    output                  valid
);

`ifdef SIM
wire[63:0]      dout_temp;

reg[63:0]       dout_ff1;
reg[63:0]       dout_ff2;
reg             en_ff1;
reg             en_ff2;
assign          dout_temp = $signed(dina) * $signed(dinb);

always@(posedge clk or negedge rst_n)
begin
    if(!rst_n)
        begin
            dout_ff1 <= 40'b0;
            dout_ff2 <= 40'b0; 
            en_ff1   <= 1'b0;
            en_ff2   <= 1'b0;
        end
    else if(!busy)
        begin
            dout_ff1 <= dout_temp ;
            dout_ff2 <= dout_ff1  ;
            en_ff1   <= en;
            en_ff2   <= en_ff1;
        end
end

assign  dout = dout_ff2;
assign  valid = en_ff2;
`elsif AISC
    initial
    begin
       $display("%m, empty module\n"); 
    end
`else
    initial
     begin
        $display("%m, empty module\n"); 
     end  
`endif 

endmodule
