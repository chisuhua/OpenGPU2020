
module sink_addr_store #
(
    parameter ADDR_WIDTH = 32,
    parameter ID_WIDTH = 4,
    parameter BL_WIDTH = 8,
    parameter ADDR_DEPTH = 2
)
(
	input                     clk,
	input                     rst_n,
    //sink
    input  [ADDR_WIDTH-1:0]   Gaddr,
    input  [BL_WIDTH-1:0]     Gbl,
    input  [ID_WIDTH-1:0]     Gid,
    input                     Gwrite,
    input                     Gvalid,
    output                    Gready, 
    //source
    output  [ADDR_WIDTH-1:0]  SGaddr,
    output  [BL_WIDTH-1:0]    SGbl,
    output  [ID_WIDTH-1:0]    SGid,
    output                    SGwrite,
    //arbiter
    output                    req,
    input                     ack
);

wire                               wr = Gvalid && Gready;
wire                               rd = ack;
wire [ADDR_WIDTH+BL_WIDTH+ID_WIDTH+1-1:0] datain = {Gaddr,Gbl,Gid,Gwrite};
wire [ADDR_WIDTH+BL_WIDTH+ID_WIDTH+1-1:0] dataout;
wire                               full;
wire                               empty;

assign {SGaddr,SGbl,SGid,SGwrite} = dataout;
assign req = !empty;
assign Gready = !full;


gpu_bus_matrix_fifo #
(
        .DWIDTH(ADDR_WIDTH+BL_WIDTH+ID_WIDTH+1),
        .DSIZE  (ADDR_DEPTH)     
) u_fifo
(
    .clk         (clk    ),
    .rst_n       (rst_n  ),
    .wr          (wr     ),
    .rd          (rd     ),
    .datain      (datain ),
    .dataout     (dataout),
    .full        (full   ),
    .empty       (empty  ),
    .almost_empty(),
    .almost_full ()
);        


endmodule
